{
"trial_search" : "Trial search",
"trial_search_results" : "Trial search results",
"trial_search_featured" : "Featured trials",

"trial_search_current" : "Current search",
"trial_search_add" : "Add search criteria",

"trial_search_title" : "Title",
"trial_search_language" : "Language",
"trial_search_authorId" : "Author ID#",
"trial_search_authorName" : "Author name",
"trial_search_featuredTrial" : "Featured trial",
"trial_search_featuredTrial_isTrue" : "Trial is featured",
"trial_search_featuredTrial_isFalse" : "Trial is not featured",
"trial_search_sequenceId" : "Sequence ID#",
"trial_search_collabsName" : "Collaborators' names",
"trial_search_collabsId" : "Collaborators' IDs#",
"trial_search_authorOrCollabsId" : "Author's or Collaborators' IDs#",

"trial_search_is" : "is exactly",
"trial_search_isNot" : "is not",
"trial_search_contains" : "contains",
"trial_search_doesNotContain" : "does not contain",
"trial_search_has" : "include exactly",
"trial_search_doesNotHave" : "do not include",
"trial_search_hasValueContaining" : "contain",
"trial_search_doesNotHaveValueContaining" : "do not contain",

"trial_search_private" : "Include private trials",

"trial_search_page" : "Page"
}
