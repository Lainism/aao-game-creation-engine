{
"trial_title" : "Titre",
"trial_author" : "Auteur",
"trial_collabs" : "Collaborateurs",
"trial_playtesters" : "Testeurs",
"trial_metadata" : "Metadonnées",

"play" : "Jouer !",
"edit" : "Editer",

"trial_sequence" : "Dans",
"trial_language" : "Langue",
"trial_creation_date" : "Créé le",
"trial_released" : "Publié",
"trial_release_date" : "Publié le",
"trial_add_collab" : "Ajouter un collaborateur",
"trial_add_playtester" : "Ajouter un testeur",
"trial_delete": "Supprimer le procès",
"trial_create": "Créer un nouveau procès",

"sequence_title": "Titre",
"sequence_metadata": "Métadonnées",
"sequence_items": "Éléments de la série",
"sequence_append_trial": "Ajouter un procès",
"sequence_delete": "Supprimer la série",
"sequence_create": "Créer une nouvelle série",

"trial_backups": "Copies de sauvegarde",
"backups_auto": "Copies automatiques",
"backups_manual": "Copies manuelles",
"backups_manual_create": "Enregistrer une copie de la version actuelle"
}
