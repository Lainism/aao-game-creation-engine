{
"error_perms_file_edit" : "No se puede editar el archivo del caso",
"error_perms_file_delete" : "No se puede eliminar el archivo del caso",

"error_perms_trial_read" : "Privilegios de usuario insuficientes para la lectura",
"error_perms_trial_manage_backups" : "Privilegios de usuario insuficientes para administrar copias de seguridad",
"error_perms_trial_edit_secured_metadata" : "Privilegios de usuario insuficientes para editar metadatos seguros",
"error_perms_trial_edit_metadata" : "Privilegios de usuario insuficientes para editar metadatos",
"error_perms_trial_create_someone_else" : "Privilegios de usuario insuficientes para crear un caso por otro usuario",

"error_perms_sequence_add_trial" : "Privilegios de usuario insuficientes para añadir este caso a una secuencia",
"error_perms_sequence_add_sequence" : "Privilegios de usuario insuficientes para añadir esta secuencia a una secuencia",
"error_perms_sequence_edit" : "Privilegios de usuario insuficientes para editar esta secuencia",
"error_perms_sequence_delete" : "Privilegios de usuario insuficientes para eliminar esta secuencia",


"error_user_inactive": "Solo un usuario identificado puede realizar esta acción",

"error_user_trial_language_unavailable" : "El idioma seleccionado no está disponible en AAO",
"error_user_trial_backup_no_slot" : "No tiene ningún espacio para copias de seguridad disponible",

"error_user_sequence_trial_already_taken" : "El caso seleccionado ya forma parte de otra secuencia",
"error_user_sequence_sequence_already_taken" : "La secuencia seleccionada ya forma parte de otra secuencia",


"error_tech_v5_trial_language_edit_in_sequence" : "El idioma de un caso dentro de una secuencia no puede ser modificado",
"error_tech_v5_trial_author_edit_in_sequence" : "El autor de un caso dentro de una secuencia no puede ser modificado",

"error_tech_v5_sequence_id_already_used" : "Ya existe una secuencia con éste nombre",
"error_tech_v5_sequence_empty" : "Una secuencia debe contener al menos un caso",
"error_tech_v5_sequence_incorrect_trial_author" : "Una seccuencia sólo puede contener casos de un mismo autor",
"error_tech_v5_sequence_incorrect_trial_language" : "Una secuencia sólo puede contener casos en un mismo idioma",
"error_tech_v5_sequence_nested" : "Las secuencias anidadas no están disponibles"
}
