{
"not_loaded" : "No se pudo cargar el caso",
"trial_edited_since_save" : "El caso ha sido editado desde que guardó. Puede que su experiencia de juego sea inconsistente.",
"trial_doesnt_match_save" : "El guardado especificado no se hizo para éste caso.",
"tech_save_env_not_initialised" : "El sistema de guardado no fue iniciado correctamente. Guardado desactivado. Por favor infórmenos de éste problema en los foros y denos un enlace a ésta pagina.",


"start" : "Empezar",
"loading_sounds" : "Cargando sonidos:",
"loading_images" : "Cargando imagenes:",
"end" : "Fin",

"press" : "Presionar",
"present" : "Presentar",
"examine" : "Examinar",
"move" : "Ir a",
"talk" : "Hablar",

"back" : "Atrás",



"profiles" : "Perfiles",
"evidence" : "Pruebas",
"select" : "Seleccionar",
"check" : "Examinar",



"player_settings" : "Opciones",
"mute" : "Silenciar",
"instant_text_typing" : "Texto instantáneo",

"player_saves" : "Partidas Guardadas",
"save_new" : "Nuevo guardado",
"save_explain" : "Las partidas son guardadas en su navegador. Si cambia de ordenador o limpia la caché de su navegador, perderá sus partidas.\n\nPara evitar esto, o compartir las partidas guardadas, puede copiar y guardar el enlace en la barra de su navegador, y almacenarlo en un lugar seguro.",
"save_error_game_not_started": "No puede guardar antesde empezar.",
"save_error_pending_timer": "No puede guardar durante un cuadro con tiempo. Espere.",
"save_error_frame_typing": "No puede guardar mientras aparece el texto. Espere.",

"player_debug" : "Depurador",

"debug_status" : "Estado",
"frame_id" : "ID# del cuadro",
"frame_index" : "Índice del cuadro",

"debug_vars" : "Variables",
"add_var" : "Definir una variable",
"var_name" : "Nombre de la variable",

"debug_cr" : "Acta de Juicio",

"debug_scenes" : "Escenas",

"debug_frames" : "Cuadros",
"show_frame" : "Ver otro cuadro"
}
