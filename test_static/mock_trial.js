/*
Mock of trial.js.php for static testing, giving all permissions and loading actual trial file.
*/

//MODULE DESCRIPTOR
Modules.load(new Object({
	name : 'trial',
	dependencies : [],
	init : function() {}
}));

//EXPORTED VARIABLES
var user_language = 'en';
var trial_information = {
	"id":42,
	"title":"Test V6",
	"author_id":2,
	"author":"Test user",
	"language":"en",
	"sequence":null,
	"can_read":true,
	"can_write":true,
	"last_edit_date":1426347856,
	"format":"Def6",
	"file_path":""
};
var initial_trial_data = null;

// Load trial file with Ajax to strip the format definition line and feed it as JSON to initial_trial_data.
var xhr = new XMLHttpRequest();
xhr.open('GET', 'test_static/mock_trial_data.txt');

xhr.onreadystatechange = function() {
	if(xhr.readyState === 4) 
	{
		var trial_file = xhr.responseText;
		
		if(trial_file.indexOf('//Definition//') === 0)
		{
			// If definition line present, check it...
			var definition = trial_file.split('\n')[0].split(/\/\//g)[2].replace(/(\r\n|\n|\r)/gm,"");
			
			if (definition !== 'Def6')
			{
				alert('Impossible to load old trial data in static mode.');
				return;
			}
			// And strip the definition line.
			var trial_file_split = trial_file.split('\n');
			trial_file_split.shift();
			initial_trial_data = JSON.parse(trial_file_split.join('\n'));
		}
		else
		{
			initial_trial_data = JSON.parse(trial_file);
		}
		trial_data = initial_trial_data;
		
		// Consider module complete after the file is loaded.
		Modules.complete('trial');
	}
};

xhr.send(null);

