This repository contains the AAO code base as well as everything needed to set up a testing environment.

There are three ways to launch AAO applications on a given machine.


1. Static testing - Quickly test changes in the editor or player
----------------------------------------------------------------

Requirements :
	Nothing specific.

Scope of test :
	Load only a single game, no saving.
	Aside from that, full edition and playthrough capabilities.
	=> Simple way of testing updates to the player or editor applications.

Instructions : 
	Just open the /editor.html and /player.html pages in your browser.
	(Provided the browser's same origin policy does not interfere : as of writing, Firefox's default policy should allow everything to work directly from the file system, while Chrome's doesn't).
	
	If you wish to test a specific game, dump it into /test_static/mock_trial_data.txt


2. Mocked install - Test all operations that can be performed by a single user
------------------------------------------------------------------------------

Requirements :
	An HTTP server configured to execute PHP, along with the PHP SQLite support module.

Scope of test :
	Game creation, edition and playthrough : all trial management operations that can be done by a user alone.
	No user account management, hence no collaboration, etc.
	
Instructions :
	- Clone this AAO repository in a directory accessible through your webserver.
	- Copy /mocked_install/config.php to /trunk/config.php
	- Access the /trunk/index.php page through your webserver.

Also, in order to avoid including the modified db.sqlite into commits by mistake, it is recommended that you run 
git update-index --assume-unchanged mocked_install/phpbb_files/db.sqlite


3. Integrated install - Test all AAO features
---------------------------------------------

Requirements :
	An HTTP server configured to execute PHP
	A MySQL database server

Scope of test :
	About all of the AAO features, including user account management.

Instructions :
	- Download phpBB3 ( https://www.phpbb.com/downloads/ ) , and extract it into a "forum" directory accessible through your webserver.
	- Follow through the phpBB3 install procedure.
	- Clone this AAO repository in a directory accessible through your webserver. If relevant, make sure it is part of the same virtual host as the forum.
	- Create the AAO tables in the database by running the SQL script /test_install/db_build.sql in the same database as the forum install.
	- Copy /test_install/config.php to /trunk/config.php
	- In this /trunk/config.php file, set the 'forum_path' property as a relative path from this file to the forum installation's root (the folder containing viewtopic.php, etc.)
	- Access the /trunk/index.php page through your webserver.
